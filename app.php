<?php

    require_once __DIR__ . '/config.php';
    require_once __DIR__ . '/vendor/autoload.php';
    require_once __DIR__ . '/CliImageDownloader.php';

    $worker = new CliImageDownloader();

    if (isset($argv[1]))
        $worker->param1 = $argv[1];
    if (isset($argv[2]))
        $worker->param2 = $argv[2];

    $worker->run();
