<?php

    use PhpAmqpLib\Connection\AMQPStreamConnection;
    use PhpAmqpLib\Message\AMQPMessage;

    class CliImageDownloader
    {
        public $param1;
        public $param2;
        private $connection;
        private $channel;
        private $timeout = 3;

        ///////////

        public function connectOpen() {
            $this->connection = new AMQPStreamConnection(
                                    RabbitMQ_HOST, RabbitMQ_PORT,
                                    RabbitMQ_USER, RabbitMQ_PASS);

            $this->channel = $this->connection->channel();
            $this->channel->queue_declare('downloader.download',
                                          false, false, false, false);
            $this->channel->queue_declare('downloader.done',
                                          false, false, false, false);
            $this->channel->queue_declare('downloader.failed',
                                          false, false, false, false);
        }

        public function connectClose() {
            $this->channel->close();
            $this->connection->close();
        }

        ///////////

        public function importFromFile() {
            echo PHP_EOL . COLOR_GREEN . "       Load URLs to download from file: ",
                 COLOR_YELLOW . $this->param2 . PHP_EOL;

            if (file_exists($this->param2)) {
                $handle = fopen($this->param2, "r");
                if ($handle) {
                    $this->connectOpen();

                    while (($line = fgets($handle)) !== false) {
                        $url = trim($line);
                        $msg = new AMQPMessage($url);

                        if (filter_var($url, FILTER_VALIDATE_URL,
                                             FILTER_FLAG_SCHEME_REQUIRED) &&
                            substr($url, 0, 4) == "http") {

                            echo COLOR_YELLOW . "[" . COLOR_GREEN . "PASS" . COLOR_YELLOW . "]";
                            $this->channel->basic_publish($msg, '', 'downloader.download');

                        } else {

                            echo COLOR_YELLOW . "[" . COLOR_RED . "FAIL" . COLOR_YELLOW . "]";
                            $this->channel->basic_publish($msg, '', 'downloader.failed');

                        }

                        echo COLOR_WHITE . " " . $url . PHP_EOL;
                    }

                    $this->connectClose();
                    fclose($handle);
                } else {
                    echo COLOR_RED . "       Error opening the file!" . PHP_EOL;
                }
            } else {
                echo COLOR_RED . "       File doesn't exist!" . PHP_EOL;
            }
        }

        ///////////

        public function callbackWaitPrint($msg) {
            echo COLOR_YELLOW . "[" . COLOR_CYAN . "WAIT" . COLOR_YELLOW . "]",
                 COLOR_WHITE . " " . $msg->body . PHP_EOL;
        }

        public function callbackDonePrint($msg) {
            echo COLOR_YELLOW . "[" . COLOR_GREEN . "DONE" . COLOR_YELLOW . "]",
                 COLOR_WHITE . " " . $msg->body . PHP_EOL;
        }

        public function callbackFailPrint($msg) {
            echo COLOR_YELLOW . "[" . COLOR_RED . "FAIL" . COLOR_YELLOW . "]",
                 COLOR_WHITE . " " . $msg->body . PHP_EOL;
        }

        public function sheduldeList() {
            echo PHP_EOL . COLOR_YELLOW . "       Schedule list:" . COLOR_WHITE . PHP_EOL;

            $this->connectOpen();

            $this->channel->basic_consume('downloader.download',
                                          '', false, false, false, false,
                                          array($this, 'callbackWaitPrint'));

            $this->channel->basic_consume('downloader.done',
                                          '', false, false, false, false,
                                          array($this, 'callbackDonePrint'));

            $this->channel->basic_consume('downloader.failed',
                                          '', false, false, false, false,
                                          array($this, 'callbackFailPrint'));

            while(count($this->channel->callbacks)) {
                try {
                    $this->channel->wait(null, false, $this->timeout);
                } catch(\PhpAmqpLib\Exception\AMQPTimeoutException $e){
                    $this->connectClose();
                    break;
                }
            }
        }

        ///////////

        public function callbackSave($msg) {
            echo COLOR_GREEN . "[" . COLOR_YELLOW . "DONE" . COLOR_GREEN . "]",
                 COLOR_WHITE . " " . $msg->body . PHP_EOL;

            $filename = basename($msg->body);

            if (file_exists(__DIR__ . '/uploads/' . $filename))
                $filename = time() . "-" . basename($msg->body);

            $result = file_put_contents(__DIR__ . '/uploads/' . $filename,
                                        file_get_contents($msg->body));

            $msg = new AMQPMessage($msg->body);

            if ($result) {
                $this->channel->basic_publish($msg, '', 'downloader.done');
            } else {
                $this->channel->basic_publish($msg, '', 'downloader.failed');
            }

        }

        public function downloadFromQueue() {
            echo PHP_EOL . COLOR_GREEN . "       Start download URLs from queue..." . PHP_EOL;

            $this->connectOpen();

            $this->channel->basic_consume('downloader.download',
                                          '', false, true, false, false,
                                          array($this, 'callbackSave'));

            while(count($this->channel->callbacks)) {
                try {
                    $this->channel->wait(null, false, $this->timeout);
                } catch(\PhpAmqpLib\Exception\AMQPTimeoutException $e){
                    $this->connectClose();
                    break;
                }
            }

            echo COLOR_GREEN . "       All images has been downloaded!" . PHP_EOL;
        }

        ///////////

        public function resetQueue() {
            echo PHP_EOL . COLOR_GREEN . "       Reset queue..." . PHP_EOL;

            $this->connectOpen();

            $this->channel->basic_consume('downloader.download',
                                          '', false, true, false, false, null);
            $this->channel->basic_consume('downloader.done',
                                          '', false, true, false, false, null);
            $this->channel->basic_consume('downloader.failed',
                                          '', false, true, false, false, null);

            while(count($this->channel->callbacks)) {
                try {
                    $this->channel->wait(null, false, $this->timeout);
                } catch(\PhpAmqpLib\Exception\AMQPTimeoutException $e){
                    $this->connectClose();
                    break;
                }
            }
        }

        public function run() {
            if (isset($this->param1)) {
                switch ($this->param1) {
                    case "schedule":
                        if (isset($this->param2)) {
                            $this->importFromFile();
                        } else {
                            $this->sheduldeList();
                        }
                        break;

                    case "download":
                        $this->downloadFromQueue();
                        break;

                    case "reset":
                        $this->resetQueue();
                        break;

                    default:
                        echo PHP_EOL;
                        echo COLOR_RED . "       Invalid parameter!" . PHP_EOL;
                        break;
                }
            } else {
                echo PHP_EOL;
                echo COLOR_RED . "       You must use parameter, eq.: ",
                     "schedule / download / reset" . PHP_EOL;
            }
            echo PHP_EOL;
        }
    }
