<?php

    define("RabbitMQ_HOST", "localhost");
    define("RabbitMQ_PORT", "5672");
    define("RabbitMQ_USER", "guest");
    define("RabbitMQ_PASS", "guest");

    define("COLOR_BLACK", "\033[0;30m");
    define("COLOR_RED", "\033[0;31m");
    define("COLOR_GREEN", "\033[0;32m");
    define("COLOR_YELLOW", "\033[0;33m");
    define("COLOR_BLUE", "\033[0;34m");
    define("COLOR_PURPLE", "\033[0;35m");
    define("COLOR_CYAN", "\033[0;36m");
    define("COLOR_WHITE", "\033[0;37m");
